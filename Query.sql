
create database ManagementSystem;

use ManagementSystem;

create table Usuario(
	id int not null auto_increment primary key,
	nombre varchar(30),
	clave varchar(30)
);

create table Producto(
	id int not null auto_increment primary key,
	nombre varchar(30),
	precio double(13,2),
	id_usuario int not null,
	foreign key(id_usuario) references Usuario(id)
);

create table DetalleProducto(
	id int not null auto_increment primary key,
	Pantalla varchar(60),
	MemoriaRam varchar(15),
	Almacenamiento varchar(15),
	Sistema varchar(30),
	Camara varchar(30),
	id_producto int not null,
	foreign key(id_producto) references Producto(id)
);


