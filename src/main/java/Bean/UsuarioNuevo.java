package Bean;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

import Entity.Usuario;

@ManagedBean
@SessionScoped
public class UsuarioNuevo {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("data");
	EntityManager em = emf.createEntityManager();
	
	private String nombre;
	private String clave;
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String addUsuario() {
		
		String respuesta = "";
		Usuario user = new Usuario();
		user.setNombre(nombre);
		user.setClave(clave);
	
		List<Usuario> listaUsuario = em.createNamedQuery("Usuario.obtenerNombre", Usuario.class)
				.setParameter("nombre", nombre)
				.getResultList();
		try {
			em.getTransaction().begin();
		if(listaUsuario.contains(user)) {
			System.out.println("Este usuario ya existe!");
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Este nombre ya esta registrado!");
			facesContext.addMessage("formregister:inputName", facesMessage);
			
			respuesta = "register.xhtml";
		}else if(listaUsuario.contains(user) != true){
			
			em.persist(user);
			System.out.println("Este usuario ya existe!");
			FacesContext facesContext = FacesContext.getCurrentInstance();
			FacesMessage facesMessage = new FacesMessage("Ha sido registrado con exito!");
			facesContext.addMessage("formindex:outputtext", facesMessage);
			System.out.println("se ingresaron los datos correctamente!");
			
			respuesta = "index.xhtml";
		}
		em.getTransaction().commit();
		}catch (Exception e) {
			// TODO: handle exception
		}
		return respuesta;
		
	}

	public String volver() {
		return "index.xhtml";
	}
	
}
