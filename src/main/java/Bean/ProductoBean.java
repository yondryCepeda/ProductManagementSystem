package Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import Entity.DetalleProducto;
import Entity.Producto;
import Entity.Usuario;

@ManagedBean
@RequestScoped
public class ProductoBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4950143645563674477L;
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("data");
	EntityManager em = emf.createEntityManager();
	
	private int id;
	private String nombre;
	private int precio;
	private String detalleProducto;
	List <Producto> listaProducto = new ArrayList();
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public String getDetalleProducto() {
		return detalleProducto;
	}
	public void setDetalleProducto(String detalleProducto) {
		this.detalleProducto = detalleProducto;
	}
	public List<Producto> getListaProducto() {
		return listaProducto;
	}
	public void setListaProducto(List<Producto> listaProducto) {
		this.listaProducto = listaProducto;
	}
	
	public List<Producto> findAll(){
		listaProducto = em.createNamedQuery("Producto.obtenerProductoNombre", Producto.class).getResultList();
		return listaProducto;
		
	}
	

}
