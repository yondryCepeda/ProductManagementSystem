package Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

import Entity.Usuario;

@ManagedBean
@SessionScoped
public class UsuarioBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1690331105794142851L;
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("data");
	EntityManager em = emf.createEntityManager();
	
	private String nombre;
	private String clave;
	private String usuario;
	List<Usuario> listaUsuario = new ArrayList();
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public List<Usuario> getLista() {
		return listaUsuario;
	}
	public void setLista(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
	
	public void listarUsuarios() {
		
		listaUsuario = em.createNamedQuery("Usuario.listaUsuario", Usuario.class).getResultList();
	 	System.out.println("lista de usuarios disponibles: " + listaUsuario);
		
		}

	
	public String loggear() {
		
		listaUsuario = em.createNamedQuery("Usuario.Login", Usuario.class)
				.setParameter("nombre", nombre)
				.setParameter("clave", clave)
				.getResultList();
		
		if(listaUsuario.isEmpty()) {
			FacesMessage fm = new FacesMessage("Usuario o clave incorrecto","error msg");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			
			return "index.xhtml";		
		}else {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", listaUsuario.get(0));
			return "wellcome?faces-redirect=true"; 	
		}
	}
	
	public String cerrar() {
			HttpSession hs = Utils.Util.getSession();
			hs.invalidate();
			return "index.xhtml";
	}
	
	//este metodo se utiliza para guardar la sesion de un usuario y no pueda volver a loggearse sin antes cerrar la session
		public void usuarioRegistrado(){
			try {
				Usuario u = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
			if(u != null) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("wellcome.xhtml");
			}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		//este metodo se utiliza para redireccionar a un usuario que aun no esta loggeado a la pagina de wellcome y pueda loggearse por medio de sus credenciales
		public void usuarioNoRegistrado(){
			
			try {
				Usuario u = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
			if(u==null) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
			}
			}catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		
		
		public String register() {
			return "register.xhtml?faces-redirect=true";
		}

	
	
	
	
}
